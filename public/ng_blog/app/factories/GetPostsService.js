blogApp.factory('GetPostsService', function($http, $q){
	var factory = {};

	factory.fetch_posts = function(post_id){
		var config = {}, url = "", deferred = $q.defer();

		if (post_id.trim().length != 0 ) {
			config.params = { post_id: post_id };
			url = "/get_post.json";
		} else{
			url = "/get_posts.json";
		}

		$http.get(url, config).success(function (data, status, headers, config) {
    	deferred.resolve(data);
  	}).error(function (data, status, headers, config) {
      deferred.reject(data);
	  });

		return deferred.promise;
	};

	return factory;
});