blogApp.controller('DisplayAllPosts', function($scope, $http, GetPostsService) {
  $scope.message = "All posts";

	/*$http.get("/get_posts.json").success(function(data) {
    $scope.posts = data;
  });*/

	var _promise = GetPostsService.fetch_posts('');

	_promise.then(function (data) {
    $scope.posts = data;
  }, function (error) {
  	console.log(error);
  });

});