blogApp.controller('SearchByTag', function($scope, $http, $routeParams) {
  $scope.message = "Tag search";

  var config = {
  	params: { tag: $routeParams.tag }
  };

	$http.get("/search_by_tag.json", config).success(function(data) {
    $scope.posts = data;
  });
});