blogApp.controller('DisplayPost', function($scope, $http, $routeParams, GetPostsService) {
  $scope.message = "Single post";

/*var config = {
  	params: { post_id: $routeParams.post_id }
  };

	$http.get("/get_post.json", config).success(function(data) {
    $scope.post = data.posts[0];
  });*/

	var _promise = GetPostsService.fetch_posts($routeParams.post_id);

	_promise.then(function (data) {
      $scope.post = data.posts[0];
  }, function (error) {
  	console.log(error);
  });

});