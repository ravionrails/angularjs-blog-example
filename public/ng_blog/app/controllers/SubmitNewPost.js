blogApp.controller('SubmitNewPost', function($scope, $http, $location) {

  $scope.message = "Submit new post";
  $scope.new_post = {
  	title: "",
  	content: "",
  	tags: ""
  };

  $scope.create_post = function(){
  	var np = $scope.new_post,
  	_data = {
  		title: np.title,
  		content: np.content,
  		tags: np.tags
  	};

  	$http.post("/create_post.json", _data).success(function(data) {
    	$location.path('/');
		});
  };

});