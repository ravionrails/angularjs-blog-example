blogApp.config(
  ['$routeProvider', function($routeProvider) {
      $routeProvider.
          when('/', {
              templateUrl: '/ng_blog/app/views/display_all_posts.html',
              controller: 'DisplayAllPosts'
          }).
          when('/submit_new_post', {
              templateUrl: '/ng_blog/app/views/submit_new_post.html',
              controller: 'SubmitNewPost'
          }).
          when('/post/:post_id', {
              templateUrl: '/ng_blog/app/views/display_post.html',
              controller: 'DisplayPost'
          }).
          when('/tag/:tag', {
              templateUrl: '/ng_blog/app/views/display_all_posts.html',
              controller: 'SearchByTag'
          }).
          otherwise({
              redirectTo: '/'
          });
  }]
);