class HomeController < ApplicationController
	include Mongo

  def index
    render "#{Rails.root}/public/ng_blog/index.html"
  end

  def get_post
  	db = db_client
  	coll = db.collection("posts")

  	if params["post_id"].blank?
  		data_arr = coll.find.to_a
      data_arr.each{|data|
        data["content"] = data["content"][0..10] + "..."
      }
  	else
  		data_arr = coll.find("_id" => BSON::ObjectId(params["post_id"])).to_a
  	end

  	data_arr.each{|data|
  		data["_id"] = data["_id"].to_s
		}

  	data_hash = {"posts" => data_arr}

  	logger.info "$$ JSON >> #{data_hash.inspect}"

  	respond_to{|format|
      format.json {render json: data_hash}
		}
  end

  def create_post
		db = db_client
  	coll = db.collection("posts")

  	doc = coll.insert({
  					"title" => params["title"],
  					"content" => params["content"],
  					"tags" => params["tags"].split(",")
  				})

  	respond_to{|format|
			format.json {render json: {message: "Successfuly posted."}}
		}
  end

  def search_by_tag
    db = db_client
    coll = db.collection("posts")

    data_cursor = coll.find({ tags: params["tag"] })

    data_arr = []
    data_hash = {}

    while data_cursor.has_next?
      data = data_cursor.next
      data["_id"] = data["_id"].to_s
      data["content"] = data["content"][0..10] + "..."
      data_arr << data
    end

    data_hash = {"posts" => data_arr}

    logger.info "$$ TAG_JSON >> #{data_hash.inspect}"

    respond_to{|format|
      format.json {render json: data_hash}
    }
  end

  private
  def db_client
		MongoClient.new("localhost", 27017).db("ng_blog")
  end
end
